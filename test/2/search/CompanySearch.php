<?php


namespace medicine\models\search;

use common\models\queries\ActiveQuery;
use medicine\models\Company;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CompanySearch extends Company
{

    /** @var ActiveQuery */
    public $query;
    public $perPage = 20;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'slug', 'personId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->query;
        if (!$query) {
            $query = static::find();
            $query->joinWith('companyPersons cp');
            $query->select(['company.*', 'COUNT(cp.id) AS personCount']);
            $query->groupBy(['company.id']);
            $query->orderBy([
                'name' => SORT_ASC,
                'personCount' => SORT_DESC,
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->perPage,
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'slug' => $this->slug,
        ]);

        if (!empty($this->id)) {
            if (is_integer($this->id)) {
                $query->andWhere(['id' => $this->id]);
            } else {
                $query->andWhere(['like', 'name', $this->id]);
            }
        }

        if (!empty($this->personId)) {
            if (is_integer($this->id)) {
                $query->joinWith(['companyPersons' => function ($q) {
                    $q->where(['personId' => $this->personId]);
                }]);
            } else {
                $query->joinWith(['persons' => function ($q) {
                    $q->where(['like', 'persons.name', $this->personId]);
                }
                ]);
            }
        }

        return $dataProvider;
    }
}